# 开发环境配置

* IDEA
    * 配置line separator为LF
        * Settings > Code Style > General > Line Separator 选择Unix and macOS(\n)
        <!-- ![img](../../_assests/crlf配置.png) -->
        ![img](./_assests/crlf配置.png)
* Git
    * 关闭自动CRLF
        * 执行git config --global core.autocrlf false
    